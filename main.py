from random import shuffle


def get_players_decks(card_list: list):
    len_card_list = len(card_list)

    if len_card_list % 2 != 0:
        return None

    shuffle(card_list)
    qtd_card_to_each_player = len_card_list // 2

    return card_list[:qtd_card_to_each_player], card_list[qtd_card_to_each_player:]


def compare_cards(cards_of_player_a, cards_of_player_b, score):
    turn_winners = []

    for index_card in range(len(cards_of_player_a)):
        card_of_player_a = cards_of_player_a[index_card]
        card_of_player_b = cards_of_player_b[index_card]
        card_winner = []

        for attributes in card_of_player_a.keys():

            if attributes != 'name':

                if card_of_player_a[attributes] > card_of_player_b[attributes]:
                    card_winner.append(1)
                elif card_of_player_a[attributes] < card_of_player_b[attributes]:
                    card_winner.append(2)

        turn_winners.append(1 if card_winner.count(1) > card_winner.count(2) else 2)

    score = turn_winners.count(1), turn_winners.count(2)

    return score


def play(card_list, score):
    cards_player_a, cards_player_b = get_players_decks(card_list)
    scores = compare_cards(cards_player_a, cards_player_b, score)
    return scores


def main():
    SUPER_HEROES = [
        {'name': 'batman', 'intelligence': 10, 'power': 1, 'strenght': 6, 'agility': 6, 'vitality': 7},
        {'name': 'super man', 'intelligence': 7, 'power': 10, 'strenght': 10, 'agility': 10, 'vitality': 10},
        {'name': 'bat girl', 'intelligence': 8, 'power': 1, 'strenght': 4, 'agility': 8, 'vitality': 3},
        {'name': 'iron man', 'intelligence': 10, 'power': 1, 'strenght': 2, 'agility': 5, 'vitality': 5}
    ]

    scores = play(SUPER_HEROES, (0, 0,))

    print(scores)


if __name__ == '__main__':
    main()
