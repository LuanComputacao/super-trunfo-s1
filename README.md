Hoje você criará um super trunfo de super-heróis. Vamos considerar cada carta do super trunfo como um dicionário em python, cada uma das cartas têm os seguintes atributos a serem considerados:

* intelligence: inteligência do super-herói
* power: seu poder sobrenatural perante os outros super-heróis
* strenght: força física do super-heroi
* agility: agilidade do super-herói
* vitality: resistência e energia do super-heroi
* Defina uma constante chamada SUPER_HEROS conforme este snippet.

# Defina as seguintes funções:

`get_players_decks(card_list)`
* Parâmetros:
    * card_list: Lista de dicionários, em que cada dicionário representa uma carta
* Procedimento:
    * Verificar se a quantidade de cartas é par, caso não seja, retornar None
    * De forma aleatória (shuffle) e igualitária dividir as cartas (dicionários) da lista card_list entre dois jogadores (embaralhar cartas para 2 jogadores)
* Retorno: Uma tupla de duas posições, a primeira para o jogador A e a segunda para o jogador B. Cada posição da tupla deve conter uma lista de dicionários, em que cada dicionário é uma  carta.

`compare_cards(card_of_player_a, card_of_player_b, score)` 
* Parâmetros:
    * card_of_player_a: Um dicionário contendo as cartas do primeiro jogador
    * card_of_player_b: Um dicionário contendo as cartas do segundo jogador
    * score Uma tupla com duas posições. Na primeira posição, um valor numérico indicando a pontuação do jogador A e a segunda posição um valor numérico indicando a pontuação do jogador B
* Procedimento:
    * Deve comparar cada uma das propriedades da carta do jogador A com a carta do jogador B
    * Para cada comparação, mostrar uma mensagem na tela (utilizando print) para mostrar, qual das duas cartas tem tal propriedade com valor maior. Veja os exemplos na seção "Entradas e Saídas"
    * Baseando-se no resultado de todas as comparações, o score do jogador com a carta vencedora deve ser incrementado em 1.
    * Em caso de empate, não incrementar o score de nenhum dos dois jogadores
* Retorno: Uma tupla de duas posições representando os scores atualizado.

`play(card_list, score)`
* Parâmetros:
    * card_list: Lista de dicionários, em que cada dicionário representa uma carta.
    * score: Uma tupla com duas posições. Na primeira posição, um valor numérico indicando a pontuação do jogador A e a segunda posição um valor numérico indicando a pontuação do jogador B.
* Procedimento:
    * Chamar a função get_players_decks para obter o deck de cada jogador.
    * Utilizar a função compare_cards para comparar as cartas dos jogadores.
    * A todas as cartas do "jogador A" deve ser comparada com a todas as cartas do "jogador B", de forma sequencial.
    * Montar uma tupla com duas posições, sendo a primera posição o total de cartas que vencedoras do "jogador A" e a segunda posição, o total de cartas vencedoras do "jogador B".
* Retorno: Uma tupla com duas posições, em sua primeira posição, o total de cartas vitoriosas do "jogador A", e na segunda posição, o total de cartas vitoriosas do "jogador B". 